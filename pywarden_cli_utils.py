from functools import wraps

import click
from pysectools.pinentry import Pinentry, PinentryErrorException

from pywarden.exceptions import PywardenException


def getpass(app_name,
            description='Your vault is locked. Verify your master password to '
                        'continue'):
    pinentry = Pinentry()
    password = pinentry.ask(prompt=f'{app_name}: enter your password',
                            description=description)
    pinentry.close()
    return password


def handle_exceptions(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except (PywardenException, PinentryErrorException) as e:
            raise click.ClickException(e)
    return wrapper
