import unittest

from pywarden.exceptions import IncorrectPasswordException
from pywarden.crypto import Crypto


class TestCrypto(unittest.TestCase):
    """
    References:
    - https://help.bitwarden.com/crypto.html
    """

    def test_master_key(self):
        email = 'user@example.com'
        password = 'password'
        iterations = 5000

        master_key = Crypto.master_key(email, password, iterations)

        self.assertIsInstance(master_key, str)
        self.assertEqual(master_key,
                         'pj9prw/OHPleXI6bRdmlaD+saJS4awrMiQsQiDjeu2I=')

    def test_master_password_hash(self):
        password = 'password'
        master_key = 'pj9prw/OHPleXI6bRdmlaD+saJS4awrMiQsQiDjeu2I='

        hashed = Crypto.master_password_hash(password, master_key)

        self.assertIsInstance(hashed, str)
        self.assertEqual(hashed,
                         '9LdZMwDKVbVzf7dJ6SbYXN3QuxnDAI0/x+5GgpFKqZ0=')

    def test_decrypt_cipher_string(self):
        string = ('2./3GUhNcWcY6FyEO6jbCuSA==|/wWObE3pqcY5WwH8kfrjwvtcZ+aGLH9C'
                  'NgciNnj9StQ=|duXXvjQBYKAJRkatav10UumuGNvfAc376lslQDJn4yA=')

        enc = '0YzWk16343zse9krY3eTd7B+IY60fCUZLn59ALCOo28='
        mac = 'hLGwUMOooNf//tu+Wush4TH8JqyUi839hPWiryQHyJQ='

        decrypted = Crypto.decrypt_cipher_string(string, enc, mac)

        self.assertIsInstance(decrypted, str)
        self.assertEqual('This is a secret.', decrypted)

    def test_decrypt_cipher_string_unequal_macs(self):
        string = ('2./3GUhNcWcY6FyEO6jbCuSA==|/wWObE3pqcY5WwH8kfrjwvtcZ+aGLH9C'
                  'NgciNnj9StQ=|duXXvjQBYKAJRkatav10UumuGNvfAc376lslQDJn4yA=')

        enc = '0YzWk16343zse9krY3eTd7B+IY60fCUZLn59ALCOo28='
        # Bogus mac
        mac = 'hLGwUMOooNf//tu+Wush4TH8JqyUi839hPWiryiiiii='

        with self.assertRaises(Exception):
            Crypto.decrypt_cipher_string(string, enc, mac)

    def test_decrypt_protected_symmetric_key(self):
        symmetric_key = ('0.QsYiYfD4CwOTjBGKD9RmuA==|v3yGpAuW4K2eQd5aFeAzFBwr8'
                         'Vi5GZZehOTUbp/1RXmg80yPyz50teKP/fkYuBM4EveYeOEHDk9qv'
                         'k7BwlxB8MMuw20eYBYmIeMyF7JdV8o=')

        master_key = 'pj9prw/OHPleXI6bRdmlaD+saJS4awrMiQsQiDjeu2I='

        enc_key, mac_key = Crypto.decrypt_protected_symmetric_key(
            symmetric_key, master_key)

        self.assertIsInstance(enc_key, str)
        self.assertEqual(
            'dN9BSBT1NbpOy8U6Wz5oLySU60IFp8uOFRk22hT2lfk=', enc_key)
        self.assertIsInstance(mac_key, str)
        self.assertEqual(
            'mUxEmQhRS4whhgBt/TWIURtJtZEnaZ2mMaTFeTs9dqk=', mac_key)

    def test_decrypt_protected_symmetric_key_incorrect_password(self):
        symmetric_key = ('0.QsYiYfD4CwOTjBGKD9RmuA==|v3yGpAuW4K2eQd5aFeAzFBwr8'
                         'Vi5GZZehOTUbp/1RXmg80yPyz50teKP/fkYuBM4EveYeOEHDk9qv'
                         'k7BwlxB8MMuw20eYBYmIeMyF7JdV8o=')

        # Bogus master key
        master_key = 'pj9prw/OHPleXI6bRdmlaD+saJS4awrMiQsQiDiiiii='

        with self.assertRaises(IncorrectPasswordException):
            enc_key, mac_key = Crypto.decrypt_protected_symmetric_key(
                symmetric_key, master_key)
