import json
import unittest
from unittest.mock import mock_open, patch
from uuid import uuid4

from pywarden.stored_object import StoredObject


class TestStoredObject(unittest.TestCase):

    @patch('pywarden.stored_object.StoredObject.load_data')
    def test_init(self, load_data_mock):
        file_path = '/a_file_path.xml'

        stored_object = StoredObject(data_file=file_path)

        self.assertEqual(stored_object.data_file, file_path)
        self.assertIsInstance(stored_object.data_dict, dict)

        load_data_mock.assert_called()

    def test_data_get(self):
        stored_object = StoredObject()

        key = uuid4()
        value = uuid4()

        stored_object.data_dict[key] = value

        data = stored_object.data

        self.assertIsInstance(data, dict)
        self.assertEqual(data[key], value)

    def test_data_set(self):
        stored_object = StoredObject()

        key = uuid4()
        value = uuid4()
        data = {key: value}

        stored_object.data = data

        self.assertIsInstance(stored_object.data_dict, dict)
        self.assertEqual(stored_object.data_dict[key], value)

    @patch('builtins.open',
           new_callable=mock_open,
           read_data='{"data":"value"}')
    def test_load_data(self, mock_file):
        file_path = '/a_file_path.xml'

        stored_object = StoredObject(data_file=None)

        stored_object.data_file = file_path
        stored_object.load_data()

        mock_file.assert_called_with(file_path, 'r')

    @patch('builtins.open')
    def test_dont_load_data(self, mock_file):
        stored_object = StoredObject(data_file=None)

        stored_object.load_data()

        mock_file.assert_not_called()

    @patch('builtins.open', side_effect=FileNotFoundError)
    def test_load_data_file_not_found(self, mock_file):
        file_path = '/a_file_path.xml'

        stored_object = StoredObject(data_file=None)

        stored_object.data_file = file_path
        stored_object.load_data()

        mock_file.assert_called_with(file_path, 'r')

    @patch('builtins.open',
           side_effect=json.decoder.JSONDecodeError('mgs', 'doc', 0))
    def test_load_data_decode_error(self, mock_file):
        file_path = '/a_file_path.xml'

        stored_object = StoredObject(data_file=None)

        stored_object.data_file = file_path
        stored_object.load_data()

        mock_file.assert_called_with(file_path, 'r')

    def test_save_data(self):
        file_path = '/a_file_path.xml'

        stored_object = StoredObject(data_file=None)
        stored_object.data_file = file_path
        stored_object.data_dict = {'data': 'value'}

        # m is needed to test if anything was actually written
        m = mock_open()
        with patch('builtins.open', m) as mock_file:
            stored_object.save_data()
            mock_file.assert_called_with(file_path, 'w')

        handle = m()
        handle.write.assert_called

    @patch('builtins.open')
    def test_dont_save_data(self, mock_file):
        StoredObject(data_file=None)

        mock_file.assert_not_called()
