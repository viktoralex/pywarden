from uuid import uuid4
import unittest
from unittest.mock import patch, PropertyMock

from pywarden.token import Token


class TestToken(unittest.TestCase):

    @patch('pywarden.stored_object.StoredObject.__init__')
    def test_init(self, stored_object_init_mock):
        file_path = '/a_file_path'

        Token(data_file=file_path)

        stored_object_init_mock.assert_called_with(data_file=file_path)

    @patch('pywarden.bitwarden_api.BitwardenApi.identity_connect_token')
    @patch('pywarden.crypto.Crypto.master_key', return_value='9999')
    @patch('pywarden.crypto.Crypto.master_password_hash',
           return_value='1234')
    @patch('pywarden.stored_object.StoredObject.data',
           new_callable=PropertyMock)
    def test_login(self,
                   data_setter_mock,
                   hash_mock,
                   master_key_mock,
                   identity_connect_token_mock):
        email = 'user@example.org'
        password = '1234'

        token = Token()
        token.login(email, password)

        data_setter_mock.assert_called()
        master_key_mock.assert_called_with(email, password)
        hash_mock.assert_called_with(password, '9999')
        identity_connect_token_mock.assert_called_with(email, '1234')

    def test_access_token(self):
        access_token = uuid4()

        token = Token()
        token.data_dict['access_token'] = access_token

        self.assertEqual(access_token, token.access_token)
