import unittest
from unittest.mock import patch, MagicMock, Mock, PropertyMock

from pywarden.pywarden import Pywarden


class TestPywarden(unittest.TestCase):

    @patch('pathlib.Path')
    def test_init_app_folder(self, pathlib_path_mock):
        app_folder = '/folder'

        path = Mock()
        path.mkdir = MagicMock()

        pathlib_path_mock.return_value = path

        Pywarden(app_folder=app_folder)

        pathlib_path_mock.assert_called_with(app_folder)
        path.mkdir.assert_called()

    @patch('pywarden.token.Token.login')
    def test_login(self, token_login_mock):
        pywarden = Pywarden()

        # fixtures
        email = 'user@example.com'
        password = 'password'

        # call function
        pywarden.login(email, password)

        # assert call passed on
        token_login_mock.assert_called_with(email, password)

    @patch('pywarden.vault.Vault.sync')
    def test_sync(self, vault_sync_mock):
        pywarden = Pywarden()

        # call function
        pywarden.sync()

        # assert call passed on
        vault_sync_mock.assert_called_with(pywarden.token)

    @patch('pywarden.vault.Vault.list_items', return_value=[1, 2, 3])
    def test_list_items(self, vault_list_items_mock):
        folder = 'folder'

        pywarden = Pywarden()

        # call function
        items = pywarden.list_items(folder_name=folder)

        # assert that it was called
        vault_list_items_mock.assert_called_with(folder)

        self.assertEqual([1, 2, 3], items)

    @patch('pywarden.vault.Vault.folders', new_callable=PropertyMock)
    def test_folders(self, vault_folders_mock):
        folders = [1, 2, 3]
        vault_folders_mock.return_value = folders

        pywarden = Pywarden()

        result = pywarden.folders

        vault_folders_mock.assert_called()

        self.assertEqual(folders, result)

    @patch('pywarden.vault.Vault.find_items', return_value=[1, 2, 3])
    def test_find_items(self, vault_find_items_mock):
        item_name = 'name'
        folder_name = 'folder'

        pywarden = Pywarden()

        items = pywarden.find_items(item_name, folder_name=folder_name)

        vault_find_items_mock.assert_called_with(
            item_name,
            folder_name=folder_name)

        self.assertEqual([1, 2, 3], items)

    @patch('pywarden.vault.Vault.unlock')
    def test_unlock(self, vault_unlock_mock):
        password = 'password'

        pywarden = Pywarden()

        pywarden.unlock(password)

        vault_unlock_mock.assert_called_with(password)
