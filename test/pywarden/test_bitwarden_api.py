import unittest
from unittest.mock import patch

from pywarden.bitwarden_api import BitwardenApi


class TestBitwardenApi(unittest.TestCase):

    @patch('requests.post')
    def test_identity_connect_token(self, post_mock):
        email = 'abc@123.is'
        hashed_password = '123123'

        BitwardenApi.identity_connect_token(email, hashed_password)

        post_mock.assert_called()
        post_args, post_kwargs = post_mock.call_args

        self.assertEqual(post_kwargs['data']['username'], email)
        self.assertEqual(post_kwargs['data']['password'], hashed_password)

    @patch('requests.get')
    def test_api_sync(self, get_mock):
        access_token = '123123'

        BitwardenApi.api_sync(access_token)

        get_mock.assert_called()
        get_args, get_kwargs = get_mock.call_args

        self.assertEqual(
            f'Bearer {access_token}',
            get_kwargs['headers']['Authorization'])
