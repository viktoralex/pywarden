from uuid import uuid4
import unittest
from unittest.mock import patch, PropertyMock

from pywarden.exceptions import FolderNotFoundException, VaultNotUnlockedException
from pywarden.vault.folder import Folder
from pywarden.vault.folder_collection import FolderCollection
from pywarden.vault.item import Item
from pywarden.vault.item_collection import ItemCollection
from pywarden.vault.profile import Profile
from pywarden.token import Token
from pywarden.vault import Vault


class TestVault(unittest.TestCase):

    @patch('pywarden.stored_object.StoredObject.__init__')
    @patch('pywarden.vault.Vault._build_from_data')
    def test_init(self, build_from_data_mock, stored_object_init_mock):
        file_path = '/a_file_path'

        Vault(data_file=file_path)

        stored_object_init_mock.assert_called_with(data_file=file_path)
        build_from_data_mock.assert_called()

    @patch('pywarden.bitwarden_api.BitwardenApi.api_sync')
    @patch('pywarden.token.Token.access_token', new_callable=PropertyMock)
    def test_sync(self, access_token_mock, api_sync_mock):
        access_token = uuid4()
        access_token_mock.return_value = access_token

        token = Token()

        vault = Vault()
        vault.sync(token)

        access_token_mock.assert_called()
        api_sync_mock.assert_called_with(access_token)

    def test_list_items(self):
        vault = Vault()

        vault.item_collection = ItemCollection([
            Item(None, {'Name': 'name1'}),
            Item(None, {'Name': 'name2'}),
            Item(None, {'Name': 'name3'})
        ])

        result = vault.list_items()

        self.assertEqual(3, len(result))

    def test_ls_with_folder(self):
        vault = Vault()

        vault.folder_collection = FolderCollection(
            [Folder(None, {'Id': 123, 'Name': 'folder_name'})])
        vault.item_collection = ItemCollection([
            Item(None, {'FolderId': 123, 'Name': 'name1'}),
            Item(None, {'FolderId': 123, 'Name': 'name2'}),
            Item(None, {'FolderId': None, 'Name': 'name3'})
        ])

        result = vault.list_items('folder_name')

        self.assertEqual(2, len(result))

    @patch('pywarden.vault.Vault._get_keys', return_value=['enc', 'mac'])
    def test_unlock(self, get_keys_mock):
        password = '1234'

        vault = Vault()

        vault.unlock(password)

        self.assertEqual('enc', vault._enc_key)
        self.assertEqual('mac', vault._mac_key)

        get_keys_mock.assert_called_with(password)

    def test_not_unlocked(self):
        vault = Vault()

        with self.assertRaises(VaultNotUnlockedException):
            vault._enc_key
        with self.assertRaises(VaultNotUnlockedException):
            vault._mac_key

    def test_find_items_name_only(self):
        name1 = 'name1'
        name2 = 'name2'
        name3 = '3name'

        vault = Vault()

        vault.item_collection = ItemCollection([
            Item(None, {'Name': name1}),
            Item(None, {'Name': name2}),
            Item(None, {'Name': name3})
        ])

        result = vault.find_items(name1)

        self.assertEqual(1, len(result))

    def test_find_items_with_folder(self):
        folder_name1 = 'folder1'
        folder_name2 = 'folder2'

        item_name = 'name'
        correct_item_id = 1

        vault = Vault()

        vault.folder_collection = FolderCollection([
            Folder(None, {'Id': 991, 'Name': folder_name1}),
            Folder(None, {'Id': 992, 'Name': folder_name2})
        ])
        vault.item_collection = ItemCollection([
            Item(None, {
                'Id': correct_item_id,
                'FolderId': 991,
                'Name': item_name
            }),
            Item(None, {'Id': 2, 'FolderId': 992, 'Name': item_name}),
            Item(None, {'Id': 3, 'FolderId': None, 'Name': item_name})
        ])

        result = vault.find_items(item_name, folder_name=folder_name1)

        self.assertEqual(1, len(result))
        self.assertEqual(result[0].identifier, correct_item_id)

    def test_find_item_with_nonexistant_folder(self):
        vault = Vault()
        self.assertRaises(FolderNotFoundException,
                          vault.find_items,
                          'an_item', folder_name='an_folder')

    @patch('pywarden.crypto.Crypto.master_key', return_value='master_key')
    @patch('pywarden.crypto.Crypto.decrypt_protected_symmetric_key',
           return_value=['enc', 'mac'])
    @patch('pywarden.vault.profile.Profile.email',
           new_callable=PropertyMock,
           return_value='email')
    @patch('pywarden.vault.profile.Profile.key',
           new_callable=PropertyMock,
           return_value='key')
    def test__get_keys(self,
                       key_mock,
                       email_mock,
                       decrypt_mock,
                       master_key_mock):
        password = 'password'

        vault = Vault()
        vault.profile = Profile({})

        enc_key, mac_key = vault._get_keys(password)

        self.assertEqual(enc_key, 'enc')
        self.assertEqual(mac_key, 'mac')

        master_key_mock.assert_called_with('email', password)
        decrypt_mock.assert_called_with('key', 'master_key')
        email_mock.assert_called()
        key_mock.assert_called()

    @patch('pywarden.crypto.Crypto.decrypt_cipher_string',
           return_value='string')
    @patch('pywarden.vault.Vault._enc_key',
           new_callable=PropertyMock,
           return_value='enc')
    @patch('pywarden.vault.Vault._mac_key',
           new_callable=PropertyMock,
           return_value='mac')
    def test_decrypt(self, mac_mock, enc_mock, decrypt_mock):
        cipher = 'cipher'

        vault = Vault()

        vault.decrypt(cipher)

        mac_mock.assert_called()
        enc_mock.assert_called()
        decrypt_mock.assert_called_with(cipher, 'enc', 'mac')

    @patch('pywarden.vault.folder_collection.FolderCollection.folders',
           new_callable=PropertyMock)
    def test_folders(self, folders_mock):
        folders = [1, 2, 3]
        folders_mock.return_value = folders

        vault = Vault()

        result = vault.folders

        folders_mock.assert_called()
        self.assertEqual(folders, result)

    @patch('pywarden.vault.item_collection.ItemCollection.items',
           new_callable=PropertyMock)
    def test_items(self, items_mock):
        items = [1, 2, 3]
        items_mock.return_value = items

        vault = Vault()

        result = vault.items

        items_mock.assert_called()
        self.assertEqual(items, result)
