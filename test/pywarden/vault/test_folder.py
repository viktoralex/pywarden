import unittest

from pywarden.vault.folder import Folder


class TestFolder(unittest.TestCase):

    def test_init(self):
        identifier = 'identifier'
        name = 'name'

        folder = Folder(None, {
            'Id': identifier,
            'Name': name
        })

        self.assertEqual(folder.identifier, identifier)
        self.assertEqual(folder.name, name)
