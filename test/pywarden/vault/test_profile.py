import unittest

from pywarden.vault.profile import Profile


class TestFolder(unittest.TestCase):

    def test_init(self):
        email = 'email'
        key = 'key'

        profile = Profile({'Email': email, 'Key': key})

        self.assertEqual(profile.email, email)
        self.assertEqual(profile.key, key)
