import unittest

from pywarden.vault.item import Item
from pywarden.vault.item_collection import ItemCollection


class TestItemCollection(unittest.TestCase):

    def test_init(self):
        items = ['a', 'b', 'c']

        item_collection = ItemCollection(items)

        self.assertEqual(len(items), len(item_collection.items))

    def test_get_item(self):
        items = ['a', 'b', 'c']

        item_collection = ItemCollection(items)

        for i, e in enumerate(items):
            self.assertEqual(e, item_collection[i])

    def test_items_by_folder(self):
        folder1_id = 1
        folder1_item_id = 8
        folder2_id = 2
        folder2_item_id = 7

        items = [
            Item(None, {'Id': folder1_item_id, 'FolderId': folder1_id }),
            Item(None, {'Id': folder2_item_id, 'FolderId': folder2_id }),
        ]

        item_collection = ItemCollection(items)

        self.assertEqual(2, len(item_collection.items))

        folder1_items = item_collection.items_by_folder(folder1_id)
        self.assertEqual(1, len(folder1_items))
        self.assertEqual(folder1_item_id, folder1_items[0].identifier)

    def test_items_by_name(self):
        folder1_id = 1
        folder1_item_id = 7
        folder1_item_name = 'item7'
        folder2_id = 2
        folder2_item_id = 8
        folder2_item_name = 'item8'

        items = [
            Item(None, {
                'Id': folder1_item_id,
                'Name': folder1_item_name,
                'FolderId': folder1_id
            }),
            Item(None, {
                'Id': folder2_item_id,
                'Name': folder2_item_name,
                'FolderId': folder2_id
            }),
        ]

        item_collection = ItemCollection(items)

        self.assertEqual(2, len(item_collection.items))

        by_name_only = item_collection.items_by_name(folder1_item_name)
        self.assertEqual(1, len(by_name_only))
        self.assertEqual(folder1_item_id, by_name_only[0].identifier)

        by_name_and_folder = item_collection.items_by_name(
            folder2_item_name, folder_id=folder2_id)
        self.assertEqual(1, len(by_name_and_folder))
        self.assertEqual(folder2_item_id, by_name_and_folder[0].identifier)
