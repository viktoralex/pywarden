import unittest
from unittest.mock import MagicMock, call

from pywarden.vault.vault import Vault
from pywarden.vault.item import Item


class TestItem(unittest.TestCase):

    def test_init(self):
        identifier = 'identifier'
        folder_id = 'folder_id'
        name = 'name'
        username = 'username'
        password = 'password'
        uri = 'uri'

        item = Item(None, {
            'Id': identifier,
            'FolderId': folder_id,
            'Name': name,
            'Login': {
                'Username': username,
                'Password': password,
                'Uri': uri,
            },
        })

        self.assertEqual(item.identifier, identifier)
        self.assertEqual(item.folder_id, folder_id)
        self.assertEqual(item.name, name)
        self.assertEqual(item.username, username)
        self.assertEqual(item.password, password)
        self.assertEqual(item.uri, uri)

    def test_get_field_names_not_decrypted(self):
        cipher_name1 = 'cipher_name1'
        decrypted_name1 = 'decrypted_name1'
        cipher_value1 = 'cipher_value1'
        decrypted_value1 = 'decrypted_value1'

        cipher_name2 = 'cipher_name2'
        decrypted_name2 = 'decrypted_name2'
        cipher_value2 = 'cipher_value2'
        decrypted_value2 = 'decrypted_value2'

        decrypted = {
            cipher_name1: decrypted_name1,
            cipher_value1: decrypted_value1,
            cipher_name2: decrypted_name2,
            cipher_value2: decrypted_value2,
        }

        def decrypt_mock_func(string):
            return decrypted.get(string)

        vault = Vault()
        vault.decrypt = MagicMock(side_effect=decrypt_mock_func)

        item = Item(vault, {
            'Fields': [
                {'Name': cipher_name1, 'Value': cipher_value1},
                {'Name': cipher_name2, 'Value': cipher_value2}
            ]
        })

        result = item.get_field(decrypted_name2)

        self.assertEqual(decrypted_value2, result)

        vault.decrypt.assert_has_calls([
            call(cipher_name1),
            call(cipher_name2),
            call(cipher_value2)
        ])

    def test_get_field_names_decrypted(self):
        decrypted_name = 'decrypted_name'
        cipher_value = 'cipher_value'
        decrypted_value = 'decrypted_value'

        vault = Vault()
        vault.decrypt = MagicMock(return_value=decrypted_value)

        item = Item(vault, {})
        item._fields = {
            decrypted_name: cipher_value,
        }

        result = item.get_field(decrypted_name)

        self.assertEqual(decrypted_value, result)

        vault.decrypt.assert_called_with(cipher_value)
