import unittest
from unittest.mock import patch

from pywarden.vault.decorators import Decrypted
from pywarden.vault.vault import Vault


class MockClass:

    def __init__(self, vault, a_property):
        self._vault = vault
        self._a_property = a_property

    @property
    @Decrypted
    def a_property(self):
        return self._a_property

    @Decrypted
    def a_function(self, string):
        return string


class TestDecrypted(unittest.TestCase):

    @patch('pywarden.vault.Vault.decrypt', return_value='decrypted_property')
    def test_property(self, decrypt_mock):
        encrypted_property = 'encrypted_property'

        vault = Vault()
        item = MockClass(vault, encrypted_property)

        result = item.a_property

        self.assertEqual('decrypted_property', result)
        decrypt_mock.assert_called_with(encrypted_property)

    @patch('pywarden.vault.Vault.decrypt', return_value='decrypted_string')
    def test_function(self, decrypt_mock):
        encrypted_string = 'encrypted_string'

        vault = Vault()
        item = MockClass(vault, None)

        result = item.a_function(encrypted_string)

        self.assertEqual('decrypted_string', result)
        decrypt_mock.assert_called_with(encrypted_string)

    @patch('pywarden.vault.Vault.decrypt', return_value='decrypted_string')
    def test_function_returning_list(self, decrypt_mock):
        encrypted_list = ['a', 'b', 'c']

        vault = Vault()
        item = MockClass(vault, None)

        result = item.a_function(encrypted_list)

        self.assertEqual(len(encrypted_list), len(result))
        self.assertEqual(3, decrypt_mock.call_count)
