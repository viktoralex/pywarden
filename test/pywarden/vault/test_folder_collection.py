import unittest

from pywarden.vault.folder import Folder
from pywarden.vault.folder_collection import FolderCollection


class TestFolderCollection(unittest.TestCase):

    def test_init(self):
        folders = ['a', 'b', 'c']

        folder_collection = FolderCollection(folders)

        self.assertEqual(len(folders), len(folder_collection.folders))

    def test_get_item(self):
        folders = ['a', 'b', 'c']

        folder_collection = FolderCollection(folders)

        for i, e in enumerate(folders):
            self.assertEqual(e, folder_collection[i])

    def test_folder_by_name(self):
        folder_id = 9
        folder_name = 'folder9'

        folders = [
            Folder(None, {
                'Id': folder_id,
                'Name': folder_name,
            }),
        ]

        folder_collection = FolderCollection(folders)

        result = folder_collection.folder_by_name(folder_name)

        self.assertEqual(folder_id, result.identifier)
