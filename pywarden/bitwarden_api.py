"""Contains a class for interfacing with the bitwarden server."""

import requests


class BitwardenApi:
    """
    Class for interfacing with the bitwarden server API.

    References:
    - https://github.com/bitwarden/server
    - https://github.com/jcs/rubywarden
    """

    @classmethod
    def identity_connect_token(cls, email, hashed_password):
        """Call the identity server and get a token."""

        params = {
            'grant_type': 'password',
            'username': email,
            'password': hashed_password,
            'scope': 'api offline_access',
            'client_id': 'browser',
            'deviceType': 14,
            'deviceIdentifier': 'e88d2091-ce4d-4ed6-87f6-f57004678719',
            'deviceName': 'pywarden',
            'devicePushToken': ''
        }

        resp = requests.post(
            'https://identity.bitwarden.com/connect/token',
            data=params)

        # raise HTTPError if unsuccessful status
        resp.raise_for_status()

        return resp.json()

    @classmethod
    def api_sync(cls, access_token):
        """Call the sync method."""

        resp = requests.get(
            'https://api.bitwarden.com/sync',
            headers={
                'Authorization': f'Bearer {access_token}'
            })

        # raise HTTPError if unsuccessful status
        resp.raise_for_status()

        return resp.json()
