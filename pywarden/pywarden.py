"""The pywarden library API."""


import os
import pathlib

from pywarden.token import Token
from pywarden.vault import Vault


class Pywarden:
    """
    Contains the bitwarden state and operations
    """

    def __init__(self, app_folder=None):
        # Create the app folder if it doesn't exist
        if app_folder is not None:
            pathlib.Path(app_folder).mkdir(parents=True, exist_ok=True)

        def filepath(filename):
            if app_folder is None:
                return None
            return os.path.join(app_folder, filename)

        # Initialize the token class with data file (if app folder is defined)
        self.token = Token(data_file=filepath('token.json'))
        self.vault = Vault(data_file=filepath('vault.json'))

    def login(self, email, password):
        """
        Call bitwarden identity server and get user token.
        """
        self.token.login(email, password)

    def sync(self):
        """
        Force synchronization with the server.
        """
        self.vault.sync(self.token)

    def list_items(self, folder_name=None):
        """
        Return list of items
        """
        return self.vault.list_items(folder_name)

    @property
    def folders(self):
        """
        Return list of folders in vault
        """
        return self.vault.folders

    def unlock(self, password):
        """
        Unlocks the vault, in memory only
        """
        self.vault.unlock(password)

    def find_items(self, item_name, folder_name=None):
        """
        Shows a specific item.
        """
        return self.vault.find_items(item_name, folder_name=folder_name)
