"""Represents the bitwarden vault."""

from pywarden.vault.vault import Vault

__all__ = ["Vault"]
