"""Contains the vault object class which items and folders inherit"""


class Decrypted:
    """
    Decorator which decrypts strings returned by functions
    Note: It accesses the vault through the `self._vault` variable
    """

    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        value = self.function(*args, **kwargs)

        # pylint: disable=protected-access
        vault = args[0]._vault

        if vault is None:
            return value

        if isinstance(value, list):
            return [vault.decrypt(s) for s in value]

        return vault.decrypt(value)

    def __get__(self, instance, owner):
        # pylint: disable=line-too-long
        """
        Fix from: https://stackoverflow.com/questions/30104047/how-can-i-decorate-an-instance-method-with-a-decorator-class
        """
        from functools import partial
        return partial(self.__call__, instance)
