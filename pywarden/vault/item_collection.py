"""Contains the class for item collections"""


class ItemCollection:
    """
    Contains a list of vault items.
    """

    def __init__(self, items):
        self._items = items

    def __getitem__(self, key):
        return self._items[key]

    def items_by_folder(self, folder_id):
        """
        Get items in a given folder
        """
        return [i for i in self._items if i.folder_id == folder_id]

    def items_by_name(self, item_name, folder_id=None):
        """
        Find items by name
        """

        items = self.items_by_folder(folder_id) if folder_id else self._items

        return [i for i in items if i.name == item_name]

    @property
    def items(self):
        """
        Get all items
        """
        return self._items
