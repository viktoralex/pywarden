"""Represents the users bitwarden profile."""


class Profile:
    """
    Describes a users profile.
    """

    def __init__(self, data):
        self._data = data

    @property
    def email(self):
        """The users email address."""
        return self._data.get('Email')

    @property
    def key(self):
        """The users private key."""
        return self._data.get('Key')
