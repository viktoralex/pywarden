"""Represents an item from the bitwarden vault."""

from pywarden.vault.decorators import Decrypted


class Item:
    """
    Describes an item in the vault.
    """

    def __init__(self, vault, data):
        self._vault = vault
        self._data = data

        self._fields = None

    @property
    def identifier(self):
        """The identifier for this item."""
        return self._data.get('Id')

    @property
    def folder_id(self):
        """The identifier for the folder this item is contained within."""
        return self._data.get('FolderId')

    @property
    @Decrypted
    def name(self):
        """The name of this item."""
        return self._data.get('Name')

    @property
    @Decrypted
    def username(self):
        """The current username."""
        return self._data.get('Login', {}).get('Username')

    @property
    @Decrypted
    def password(self):
        """The current username."""
        return self._data.get('Login', {}).get('Password')

    @property
    @Decrypted
    def uri(self):
        """The current username."""
        return self._data.get('Login', {}).get('Uri')

    @Decrypted
    def get_field(self, name):
        """Get a value from a given field."""
        if self._fields is None:
            fields_data = self._data.get('Fields', [])

            self._fields = {}
            for field in fields_data:
                decrypted_key = self._vault.decrypt(field.get('Name'))
                self._fields[decrypted_key] = field.get('Value')

        return self._fields.get(name)
