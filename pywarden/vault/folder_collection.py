"""Contains the class for folder collections"""

from pywarden.exceptions import FolderNotFoundException


class FolderCollection:
    """
    Contains a list of vault folders.
    """

    def __init__(self, folders):
        self._folders = folders

    def __getitem__(self, key):
        return self._folders[key]

    def folder_by_name(self, folder_name):
        """
        Find a folder by name
        """
        for folder in self._folders:
            if folder.name == folder_name:
                return folder
        raise FolderNotFoundException(folder_name)

    @property
    def folders(self):
        """
        Get all folders
        """
        return self._folders
