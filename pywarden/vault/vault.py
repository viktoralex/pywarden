"""Represents the bitwarden vault."""

from pywarden.crypto import Crypto
from pywarden.bitwarden_api import BitwardenApi
from pywarden.exceptions import VaultNotUnlockedException
from pywarden.stored_object import StoredObject
from pywarden.vault.folder import Folder
from pywarden.vault.folder_collection import FolderCollection
from pywarden.vault.item import Item
from pywarden.vault.item_collection import ItemCollection
from pywarden.vault.profile import Profile


class Vault(StoredObject):
    """
    Contains the bitwarden vault and operations relating to it.
    """

    def __init__(self, data_file=None):
        super().__init__(data_file=data_file)

        self._build_from_data()

        self.__enc_key = None
        self.__mac_key = None

    def sync(self, token):
        """
        Fetches the vault state from the server and saves it.
        """

        self.data = BitwardenApi.api_sync(token.access_token)

        self._build_from_data()

    def list_items(self, folder_name=None):
        """
        Get a list of items (in a folder, if specified)
        """

        if folder_name is None:
            return self.item_collection.items

        folder_id = self.folder_collection.folder_by_name(folder_name).identifier

        return self.item_collection.items_by_folder(folder_id)

    def unlock(self, password):
        """
        Unlock in-memory vault for reading
        """
        self.__enc_key, self.__mac_key = self._get_keys(password)

    def find_items(self, item_name, folder_name=None):
        """
        Find a specific item by the name of the item.
        """

        if folder_name is None:
            return self.item_collection.items_by_name(item_name)

        folder_id = self.folder_collection.folder_by_name(folder_name).identifier

        return self.item_collection.items_by_name(item_name, folder_id=folder_id)

    @property
    def _enc_key(self):
        if self.__enc_key is None:
            raise VaultNotUnlockedException()
        return self.__enc_key

    @property
    def _mac_key(self):
        if self.__mac_key is None:
            raise VaultNotUnlockedException()
        return self.__mac_key

    @property
    def folders(self):
        """
        Get all folders in the vault
        """
        return self.folder_collection.folders

    @property
    def items(self):
        """
        Get all items in the vault
        """
        return self.item_collection.items

    def _build_from_data(self):
        self.profile = Profile(self.data.get('Profile', {}))
        self.folder_collection = FolderCollection(
            [Folder(self, f) for f in self.data.get('Folders', [])])
        self.item_collection = ItemCollection(
            [Item(self, c) for c in self.data.get('Ciphers', [])])

    def _get_keys(self, password):
        master_key = Crypto.master_key(
            self.profile.email,
            password)

        return Crypto.decrypt_protected_symmetric_key(
            self.profile.key,
            master_key)

    def decrypt(self, string):
        """Decrypt a given string using the logged in users keys."""
        return Crypto.decrypt_cipher_string(
            string,
            self._enc_key,
            self._mac_key)
