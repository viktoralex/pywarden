"""Represents a folder from the bitwarden vault."""

from pywarden.vault.decorators import Decrypted


class Folder:
    """
    Describes a folder in the vault.
    """

    def __init__(self, vault, data):
        self._vault = vault
        self._data = data

    @property
    def identifier(self):
        """The identifier for this folder."""
        return self._data.get('Id')

    @property
    @Decrypted
    def name(self):
        """The name of this folder."""
        return self._data.get('Name')
