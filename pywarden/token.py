"""Contains the bitwarden token."""


from pywarden.crypto import Crypto
from pywarden.bitwarden_api import BitwardenApi
from pywarden.stored_object import StoredObject


class Token(StoredObject):
    """
    Contains the bitwarden user token and operations relating to the user.
    """

    def __init__(self, data_file=None):
        """
        Initializes the token state.

        Reads state from file if given.
        """

        super().__init__(data_file=data_file)

    def login(self, email, password):
        """
        Logs a user in with the given email and password
        """

        master_key = Crypto.master_key(email, password)
        hashed_password = Crypto.master_password_hash(password, master_key)

        self.data = BitwardenApi.identity_connect_token(email, hashed_password)

    @property
    def access_token(self):
        """The logged in users access token."""
        return self.data.get('access_token')
