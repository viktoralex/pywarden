"""Contains the exceptions which may occur in the library."""


class PywardenException(Exception):
    """General exception that all custom exceptions inherit form."""


class IncorrectPasswordException(PywardenException):
    """Thrown when the password given is incorrect."""


class VaultObjectNotFoundException(PywardenException):
    """Thrown when a vault object could not be found."""
    def __init__(self, object_type, object_name):
        super().__init__(
            f'{object_type} {object_name} not found in vault')


class FolderNotFoundException(VaultObjectNotFoundException):
    """Thrown when a folder cannot be found in the vault."""
    def __init__(self, folder_name):
        super().__init__('Folder', folder_name)


class VaultNotUnlockedException(PywardenException):
    """Thrown when the vault is accessed when not unlocked."""
    def __init__(self):
        super().__init__('Vault not unlocked!')
