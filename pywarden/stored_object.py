"""Contains a class for accessing the stored information."""


import json


class StoredObject:
    """
    Abstract object which saves its data to a file in json format.

    Data is stored in self.data_dict in memory and when self.data is changed
    the data is saved to disk into the self.data_file file.
    """

    def __init__(self, data_file=None):
        """
        Set the data file and try to load the data from the given file.
        """
        self.data_file = data_file
        self.data_dict = {}
        self.load_data()

    @property
    def data(self):
        """
        Return the data.
        """
        return self.data_dict

    @data.setter
    def data(self, data):
        """
        Update the data dictionary and save it to file.
        """
        self.data_dict = data
        self.save_data()

    def load_data(self):
        """Load data from a file"""
        if self.data_file is None:
            return

        try:
            with open(self.data_file, 'r') as file:
                self.data_dict = json.load(file)
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            pass

    def save_data(self):
        """Save data to a file"""
        if self.data_file is None:
            return

        with open(self.data_file, 'w') as file:
            json.dump(self.data_dict, file)
