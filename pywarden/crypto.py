"""Contains all methods for dealing with the bitwarden cryptography"""

import base64
import hashlib
import hmac

from Crypto.Cipher import AES

from pywarden.exceptions import IncorrectPasswordException


class Crypto:
    """
    References:
    - https://help.bitwarden.com/crypto.html
    - https://github.com/jcs/rubywarden/blob/master/API.md
    """

    @classmethod
    def master_key(cls, email, password, iterations=5000):
        """Get the master key from the users password."""
        email = bytes(email.lower(), 'utf-8')
        password = bytes(password, 'utf-8')
        master_key = cls._make_key(email, password, iterations)
        return base64.b64encode(master_key).decode('utf-8')

    @classmethod
    def master_password_hash(cls, password, master_key):
        """Get the master password hash."""
        password = bytes(password, 'utf-8')
        master_key = cls._b64decode(master_key)
        hashed_password = cls._make_key(password, master_key, 1)
        return base64.b64encode(hashed_password).decode('utf-8')

    @classmethod
    def decrypt_cipher_string(cls, string, enc_key, mac_key):
        """Decrypt a string."""
        initialization_vector, cipher_text, mac = string[2:].split('|')

        enc_key, mac_key, initialization_vector, cipher_text, mac = \
            cls._b64decode(
                enc_key,
                mac_key,
                initialization_vector,
                cipher_text,
                mac)

        cmac = hmac.new(
            key=mac_key,
            msg=initialization_vector+cipher_text,
            digestmod=hashlib.sha256).digest()

        if not cls._macs_equal(mac_key, mac, cmac):
            raise Exception('MACs not equal')

        decrypted = cls._aes_cbc_decrypt(
            enc_key,
            initialization_vector,
            cipher_text)
        return decrypted.decode('utf-8')

    @classmethod
    def decrypt_protected_symmetric_key(cls, symmetric_key, master_key):
        """Decrypt the symmetric key."""
        initialization_vector, cipher_text = symmetric_key[2:].split('|')

        master_key, initialization_vector, cipher_text = \
            cls._b64decode(
                master_key,
                initialization_vector,
                cipher_text)

        decrypted = cls._aes_cbc_decrypt(
            master_key,
            initialization_vector,
            cipher_text)

        if not decrypted:
            raise IncorrectPasswordException('Incorrect password')

        enc_key = decrypted[:len(decrypted)//2]
        mac_key = decrypted[len(decrypted)//2:]

        return (base64.b64encode(enc_key).decode('utf-8'),
                base64.b64encode(mac_key).decode('utf-8'))

    @classmethod
    def _make_key(cls, salt, password, iterations):
        return hashlib.pbkdf2_hmac(
            'sha256', password, salt, iterations, dklen=256/8)

    @classmethod
    def _macs_equal(cls, mac_key, mac1, mac2):
        hmac1 = hmac.new(
            key=mac_key,
            msg=mac1,
            digestmod=hashlib.sha256).digest()
        hmac2 = hmac.new(
            key=mac_key,
            msg=mac2,
            digestmod=hashlib.sha256).digest()

        return hmac1 == hmac2

    @classmethod
    def _b64decode(cls, *args):
        if len(args) == 1:
            return base64.b64decode(args[0])

        return [base64.b64decode(s) for s in args]

    @classmethod
    def _aes_cbc_decrypt(cls, key, initialization_vector, text):
        encryption_suite = AES.new(key, AES.MODE_CBC, initialization_vector)
        decrypted = encryption_suite.decrypt(text)
        return decrypted[:-decrypted[-1]]  # remove padding and return
