Pywarden
========

CLI tool for bitwarden.

## Usage

I want this to be used in the same way as the `lpass` cli tool.

### help and version

```
pw --help
```
Show help message for `pw`

```
pw --version
```
Show the current version of `pw`

### login and logout

```
pw login EMAIL
```
Log in the user with given email, prompts for password

```
pw logout
```
Log the user out

### show

```
pw show IDENTIFIER
```
Show the item that matches the given identifier. Identifier can be folder_name/item_name or just the item_name if it is unique. If more than one item match the identifier a list of possible matches is shown.

### list

```
pw ls [FOLDER]
```
List items in database, if folder is given then only the items in the folder are listed.

### sync

```
pw sync
```
Force a sync to the server.

### others

```
lpass passwd
lpass mv [--color=auto|never|always] {UNIQUENAME|UNIQUEID} GROUP
lpass add [--sync=auto|now|no] [--non-interactive] [--color=auto|never|always] {--username|--password|--url|--notes|--field=FIELD|--note-type=NOTETYPE} NAME
lpass edit [--sync=auto|now|no] [--non-interactive] [--color=auto|never|always] {--name|--username|--password|--url|--notes|--field=FIELD} {NAME|UNIQUEID}
lpass generate [--sync=auto|now|no] [--clip, -c] [--username=USERNAME] [--url=URL] [--no-symbols] {NAME|UNIQUEID} LENGTH
lpass duplicate [--sync=auto|now|no] [--color=auto|never|always] {UNIQUENAME|UNIQUEID}
lpass rm [--sync=auto|now|no] [--color=auto|never|always] {UNIQUENAME|UNIQUEID}
lpass status [--quiet, -q] [--color=auto|never|always]
lpass export [--sync=auto|now|no] [--color=auto|never|always] [--fields=FIELDLIST]
lpass import [--keep-dupes] [CSV_FILENAME]
lpass share subcommand sharename ...
```
